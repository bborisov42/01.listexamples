package com.innovator.listexamples.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 5/1/2018.
 */
public class NotesDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "notes.db";
    private static final int DATABASE_VERSION = 1;

    public NotesDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = "CREATE TABLE " +
                NoteContract.TABLE_NAME + "(" +
                    NoteContract._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    NoteContract.COLUMN_NAME_CONTENT + " TEXT NOT NULL " +
                ")";

        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE " + NoteContract.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public long insertNote(Note note) {
        long insertedId;
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(NoteContract.COLUMN_NAME_CONTENT, note.getContent());
        insertedId = db.insert(NoteContract.TABLE_NAME, null, cv);

        db.close();

        return insertedId;
    }

    public List<Note> getNotes() {
        List<Note> notes = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(NoteContract.TABLE_NAME,
                null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(NoteContract._ID));
            String content = cursor.getString(cursor.getColumnIndex(NoteContract.COLUMN_NAME_CONTENT));
            notes.add(new Note(id, content));
        }

        cursor.close();
        db.close();

        return notes;
    }

    public int deleteNote(int id) {
        int deletedCount;
        SQLiteDatabase db = getWritableDatabase();

        deletedCount = db.delete(NoteContract.TABLE_NAME, NoteContract._ID + " = ?", new String[] { "" + id});

        db.close();

        return deletedCount;
    }
}
