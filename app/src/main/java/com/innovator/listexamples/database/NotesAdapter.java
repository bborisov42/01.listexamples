package com.innovator.listexamples.database;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.innovator.listexamples.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 5/1/2018.
 */
public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NoteViewHolder> {

    private List<Note> notes = new ArrayList<>();
    private OnNoteClickListener listener;

    public void setOnNoteClickListener(OnNoteClickListener listener) {
        this.listener = listener;
    }

    public void addNote(Note note) {
        this.notes.add(note);
        notifyDataSetChanged();
    }

    public void addNotes(List<Note> notes) {
        this.notes.addAll(notes);
        notifyDataSetChanged();
    }

    public void removeNote(Note note) {
        this.notes.remove(note);
        notifyDataSetChanged();
    }

    public void clearNotes() {
        this.notes.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_list_item, parent, false);
        return new NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        Note note = notes.get(position);
        holder.bind(note);
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {

        TextView idTextView;
        TextView contentTextView;

        public NoteViewHolder(View itemView) {
            super(itemView);

            idTextView = itemView.findViewById(R.id.note_id_tv);
            contentTextView = itemView.findViewById(R.id.note_content_tv);
        }

        public void bind(final Note note) {
            idTextView.setText("" + note.getId());
            contentTextView.setText(note.getContent());

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (listener != null) {
                        listener.onNoteClicked(note);
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    public interface OnNoteClickListener {
        void onNoteClicked(Note note);
    }
}
