package com.innovator.listexamples.database;

/**
 * Created on 5/1/2018.
 */
public class Note {
    private int id;
    private String content;

    public Note(String content) {
        this.content = content;
    }

    public Note(int id, String content) {
        this.id = id;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
