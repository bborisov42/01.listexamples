package com.innovator.listexamples.database;

import android.provider.BaseColumns;

/**
 * Created on 5/1/2018.
 */
public class NoteContract implements BaseColumns {
    public static final String TABLE_NAME = "notes";

    public static final String COLUMN_NAME_CONTENT = "content";
}
