package com.innovator.listexamples.database;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.innovator.listexamples.R;

import java.util.List;

public class DatabaseActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Note>>,
        NotesAdapter.OnNoteClickListener {

    private static final int NOTES_LOADER_ID = 42;

    private EditText inputEditText;
    private Button submitButton;
    private RecyclerView recyclerView;
    private NotesAdapter notesAdapter;
    private NotesDbHelper notesDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);

        notesDbHelper = new NotesDbHelper(this);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        notesAdapter = new NotesAdapter();
        notesAdapter.setOnNoteClickListener(this);
        recyclerView.setAdapter(notesAdapter);

        inputEditText = findViewById(R.id.input_et);
        submitButton = findViewById(R.id.submit_btn);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CharSequence content = inputEditText.getText();
                if (!TextUtils.isEmpty(content)) {
                    Note note = new Note(content.toString());
                    int id = (int)notesDbHelper.insertNote(note);
                    note.setId(id);
                    notesAdapter.addNote(note);
                    inputEditText.setText("");
                }
            }
        });

        getSupportLoaderManager().initLoader(NOTES_LOADER_ID, null, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        notesDbHelper.close();
    }

    @Override
    public Loader<List<Note>> onCreateLoader(int i, Bundle bundle) {
        return new NotesLoader(this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<Note>> loader, List<Note> data) {
        notesAdapter.clearNotes();
        notesAdapter.addNotes(data);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<Note>> loader) {

    }

    @Override
    public void onNoteClicked(Note note) {
        notesDbHelper.deleteNote(note.getId());
        notesAdapter.removeNote(note);
    }

    private static class NotesLoader extends AsyncTaskLoader<List<Note>> {
        private List<Note> notes;

        public NotesLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            if (notes != null) {
                deliverResult(notes);
            } else {
                forceLoad();
            }
        }

        @Override
        public List<Note> loadInBackground() {
            NotesDbHelper dbHelper = new NotesDbHelper(getContext());
            return dbHelper.getNotes();
        }

        @Override
        public void deliverResult(List<Note> data) {
            notes = data;
            super.deliverResult(data);
        }
    }
}
