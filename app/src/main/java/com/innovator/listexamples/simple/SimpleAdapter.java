package com.innovator.listexamples.simple;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.innovator.listexamples.R;

/**
 * Created on 5/1/2018.
 */
public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.SimpleViewHolder> {

    private String[] items;

    public SimpleAdapter(String[] items) {
        this.items = items;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        String item = items[position];
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return items.length;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        TextView itemTextView;

        public SimpleViewHolder(View itemView) {
            super(itemView);

            itemTextView = itemView.findViewById(R.id.list_item);
        }

        public void bind(String item) {
            itemTextView.setText(item);
        }
    }
}
