package com.innovator.listexamples.network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created on 5/1/2018.
 */
public class JsonUtils {
    public static String[] parseWords(String jsonContent) {
        String[] wordNames = null;

        try {
            JSONArray jsonArray = new JSONArray(jsonContent);

            wordNames = new String[jsonArray.length()];

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.optJSONObject(i);
                wordNames[i] = jsonObject.optString("word");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return wordNames;
    }
}
