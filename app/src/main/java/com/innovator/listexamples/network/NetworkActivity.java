package com.innovator.listexamples.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.innovator.listexamples.R;
import com.innovator.listexamples.simple.SimpleAdapter;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NetworkActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String[]> {

    private static final int WORDS_LOADER = 42;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        initLoader();
    }

    private void initLoader() {
        if (NetworkUtils.isNetworkAvailable(this)) {
            getSupportLoaderManager().initLoader(WORDS_LOADER, null, this);
        } else {
            Toast.makeText(this, "Error. No Internet connection!", Toast.LENGTH_SHORT).show();
        }
    }

    @NonNull
    @Override
    public Loader<String[]> onCreateLoader(int id, @Nullable Bundle args) {
        return new WordsLoader(this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<String[]> loader, String[] data) {
        recyclerView.setAdapter(new SimpleAdapter(data));
    }

    @Override
    public void onLoaderReset(@NonNull Loader<String[]> loader) {

    }

    static class WordsLoader extends AsyncTaskLoader<String[]> {

        private String[] words;

        public WordsLoader(@NonNull Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            if (words != null) {
                deliverResult(words);
            } else {
                forceLoad();
            }
        }

        @Nullable
        @Override
        public String[] loadInBackground() {
            String[] result = null;

            String baseAddress = "http://api.wordnik.com";

            List<String> paths = new ArrayList<>();
            paths.add("v4");
            paths.add("words.json");
            paths.add("randomWords");

            Map<String, String> params = new HashMap<>();
            params.put("api_key", "a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5");

            URL url = NetworkUtils.buildUrl(baseAddress, paths, params);
            try {
                String wordsContent = NetworkUtils.getResponseFromHttpGetUrl(url);
                result = JsonUtils.parseWords(wordsContent);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        public void deliverResult(String[] data) {
            words = data;
            super.deliverResult(data);
        }
    }
}
